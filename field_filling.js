//Заполнение игрового поля
const cells = document.querySelectorAll(".cell");

const playerX = {
    playerXXL: null,
    playerIcon: null
};

const playerO = {
    playerXXL: null,
    playerIcon: null
};

playerX.playerXXL = document.createElement("img");
playerX.playerXXL.setAttribute("src", "../imgs/xxl-x.svg");

playerX.playerIcon = document.createElement("img");
playerX.playerIcon.setAttribute("src", "../imgs/x.svg");

playerO.playerXXL = document.createElement("img");
playerO.playerXXL.setAttribute("src", "../imgs/xxl-zero.svg");

playerO.playerIcon = document.createElement("img");
playerO.playerIcon.setAttribute("src", "../imgs/zero.svg");

let mode = playerX;

function changeActivePlayer() {
    if (mode === playerX) {
        setMode(playerO)
    } else {
        setMode(playerX)
    }
}

function setMode(newMode) {
    mode = newMode;
}

cells.forEach((cell) => {
    cell.addEventListener("click", () => {
        if (!cell.innerHTML) {
            cell.innerHTML = mode.playerXXL.outerHTML;
            changeActivePlayer()
            document.getElementById("game-step").innerHTML = `Ходит ${mode.playerIcon.outerHTML} игрок`;
        }
    });
});
