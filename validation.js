const loginInput = document.getElementById("login");
const passwordInput = document.getElementById("password");
const submitInput = document.getElementById("submit");

//Валидация
loginInput.addEventListener("input", () => {
    loginInput.value = loginInput.value.replace(/[^a-zA-Z0-9._]/g, "");
});

passwordInput.addEventListener("input", () => {
    passwordInput.value = passwordInput.value.replace(/[^a-zA-Z0-9._]/g, "");
});

//Запрет копирования
loginInput.addEventListener('copy',
    function(e){e.preventDefault()});

passwordInput.addEventListener('copy',
    function(e){e.preventDefault()});

//Перекрашивание кнопки, при заполненных полях
loginInput.addEventListener("input", validateInputs);
passwordInput.addEventListener("input", validateInputs);

function validateInputs() {
    if (loginInput.value !== "" && passwordInput.value !== "") {
        submitInput.style.backgroundColor = "#60C2AA";
    }
    else {
        submitInput.style.backgroundColor = "";
    }
}

const clickOnButton = () => {
    console.log(`Логин: ${loginInput.value}\nПароль: ${passwordInput.value}`)
}